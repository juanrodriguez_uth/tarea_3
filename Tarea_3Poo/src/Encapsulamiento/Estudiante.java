
package Encapsulamiento;
public class Estudiante {
    protected String nombre;
    protected int edad;
    protected int anioExp;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getAnioExp() {
        return anioExp;
    }

    public void setAnioExp(int anioExp) {
        this.anioExp = anioExp;
    }
    
}
