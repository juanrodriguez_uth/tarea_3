package tarea_3poo;
import Encapsulamiento.Estudiante;
public class Principal {
    public Principal(){
        //Llamado de clases Abstractas
        UNAH unah = new UNAH();
        UPNFM upn = new UPNFM();
        UTH uth = new UTH();
        System.out.println("___ABSTRACCION___\nRectoria UTH: "+uth.getRector());
    System.out.println("Rectoria UPNFM: "+upn.getRector());
    System.out.println("Rectoria UNAH: "+unah.getRector());
    
    //Llamado de la clase de Encapsulamiento
    Estudiante est=new Estudiante();
    est.setNombre("JUAN RODRIGUEZ");
    est.setEdad(28);
    est.setAnioExp(12);
     System.out.println("\n\n___ENCAPSULAMIENTO___\nNombre: "+est.getNombre()+"\nEdad: "+est.getEdad()+"\nAnios Experiencia: "+est.getAnioExp());
    }
    public static void main(String args[]){
        new Principal();
    }
}
